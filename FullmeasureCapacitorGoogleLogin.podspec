
  Pod::Spec.new do |s|
    s.name = 'FullmeasureCapacitorGoogleLogin'
    s.version = '1.0.3'
    s.summary = 'Capacitor plugin to authenticate with google'
    s.license = 'MIT'
    s.homepage = 'https://gitlab.com/fullmeasure/public/capacitor-google-login'
    s.author = 'Full Measure'
    s.source = { :git => 'https://gitlab.com/fullmeasure/public/capacitor-google-login.git', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.static_framework = true
    s.dependency 'Capacitor'
    s.dependency 'GoogleSignIn'
  end
